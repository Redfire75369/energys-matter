package wtf.gofancy.mc.ematter

import net.minecraft.creativetab.CreativeTabs
import net.minecraft.item.ItemStack
import net.minecraft.util.ResourceLocation
import net.minecraftforge.common.MinecraftForge
import net.minecraftforge.fml.common.Mod
import net.minecraftforge.fml.common.event.FMLConstructionEvent
import net.minecraftforge.fml.common.event.FMLInitializationEvent
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent
import net.minecraftforge.fml.common.network.NetworkRegistry
import org.apache.logging.log4j.Level
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.MarkerManager
import wtf.gofancy.mc.ematter.client.CustomHighlightManager
import wtf.gofancy.mc.ematter.common.network.GuiHandler
import wtf.gofancy.mc.ematter.features.ModFeature
import wtf.gofancy.mc.ematter.features.mad.MolecularAssemblerDevice
import wtf.gofancy.mc.ematter.features.mad.common.MadTier
import wtf.gofancy.mc.ematter.util.clientOnly

@Mod(
    modid = MOD_ID,
    name = MOD_NAME,
    version = MOD_VERSION,
    dependencies = "required:forge@[14.23.5.2847,);",
    acceptedMinecraftVersions = "1.12.2",
    modLanguageAdapter = "net.shadowfacts.forgelin.KotlinAdapter",
    modLanguage = "kotlin"
)
object BaseMod {

    private val l = LogManager.getLogger(MOD_NAME)

    internal val itemGroup: CreativeTabs = object : CreativeTabs("$MOD_ID.main") {
        // todo: determine the item to show based on the available features
        override fun createIcon() = ItemStack(MolecularAssemblerDevice.block, 1, MadTier.ELITE.targetMeta)
        override fun hasSearchBar() = true
        override fun getSearchbarWidth() = 73
        override fun getBackgroundImage() = ResourceLocation(MOD_ID, "textures/gui/item_group/main.png")
    }

    // todo: extract this into a FeatureManager class that also provides a list of selected features
    private val features = listOf(MolecularAssemblerDevice)

    @Suppress("Unused_Parameter")
    @Mod.EventHandler
    fun onModConstruction(e: FMLConstructionEvent) {
        l.log(Level.INFO, MarkerManager.getMarker("lifecycle"), "Hello there.")

        /*
         * The [RegistryEvent.NewRegistry] event is fired before pre-initialization, so we need to register the handler
         * here.
         */
        val customRegistryBuilders = this.features.flatMap(ModFeature::customRegistryBuilders)
        MinecraftForge.EVENT_BUS.apply {
            register(CustomRegistryRegistrar(customRegistryBuilders))
        }
    }

    @Suppress("Unused_Parameter")
    @Mod.EventHandler
    fun onModPreInitialization(e: FMLPreInitializationEvent) {
        // todo: read configuration to determine which features to enable

        this.features.forEach {
            /*
             * Registry population runs after pre-initialization, so we need to ensure all [RegistryEvent.Register]
             * handlers are registered before that happens.
             */
            it.registerRegistrars()

            /*
             * The Forge docs for 1.12.2 explicitly mention capability registration as something to do during
             * pre-initialization.
             */
            it.registerCapabilities()

            /*
             * To cite Silk:
             * > Since model loading happens between pre-init and init, I'm assuming this is where model loaders
             * > should be registered
             *
             * todo: check if the above statement is correct
             */
            clientOnly { { it.client()().registerModelRegistrar() } }
        }
    }

    @Suppress("Unused_Parameter")
    @Mod.EventHandler
    fun onModInitialization(e: FMLInitializationEvent) {
        // todo: parameterize GuiHandler with information from enabled features
        NetworkRegistry.INSTANCE.registerGuiHandler(BaseMod, GuiHandler())

        clientOnly {
            {
                val highlightProviders = this.features.flatMap { it.client()().customHighlightProviders }

                MinecraftForge.EVENT_BUS.apply {
                    register(CustomHighlightManager(highlightProviders))
                }
            }
        }

        this.features.forEach {
            it.registerEventHandlers()

            it.registerNetworkMessages()

            clientOnly { { it.client()().registerBlockEntityRenderers() } }
        }
    }
}
