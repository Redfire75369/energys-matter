@file:JvmName("Constants")

package wtf.gofancy.mc.ematter

import wtf.gofancy.mc.generated.InjectedTags

const val MOD_ID = InjectedTags.MOD_ID
const val MOD_NAME = InjectedTags.MOD_NAME
const val MOD_VERSION = InjectedTags.MOD_VERSION
