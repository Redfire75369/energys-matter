@file:JvmName("CommonRenderingMethods")

package wtf.gofancy.mc.ematter.client

import net.minecraft.client.renderer.GlStateManager

internal inline fun withMatrix(block: () -> Unit) {
    GlStateManager.pushMatrix()
    block()
    GlStateManager.popMatrix()
}