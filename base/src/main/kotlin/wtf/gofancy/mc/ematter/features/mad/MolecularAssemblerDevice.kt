package wtf.gofancy.mc.ematter.features.mad

import net.minecraft.block.Block
import net.minecraft.item.Item
import net.minecraft.item.ItemMultiTexture
import net.minecraft.util.ResourceLocation
import net.minecraftforge.common.MinecraftForge
import net.minecraftforge.event.RegistryEvent
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent
import net.minecraftforge.fml.common.registry.GameRegistry
import net.minecraftforge.fml.relauncher.Side
import net.minecraftforge.registries.IForgeRegistry
import net.minecraftforge.registries.RegistryBuilder
import net.minecraftforge.registries.RegistryManager
import wtf.gofancy.mc.ematter.BaseMod
import wtf.gofancy.mc.ematter.MOD_ID
import wtf.gofancy.mc.ematter.common.network.ModNetworkManager
import wtf.gofancy.mc.ematter.features.ModFeature
import wtf.gofancy.mc.ematter.features.mad.client.MolecularAssemblerDeviceClient
import wtf.gofancy.mc.ematter.features.mad.common.MadBlock
import wtf.gofancy.mc.ematter.features.mad.common.MadBlockEntity
import wtf.gofancy.mc.ematter.features.mad.common.MadTier
import wtf.gofancy.mc.ematter.features.mad.common.network.MadRecipeCapabilitySyncPacket
import wtf.gofancy.mc.ematter.features.mad.common.network.MadRecipeCapabilitySyncPacketHandler
import wtf.gofancy.mc.ematter.features.mad.common.network.MadRecipeSwitchButtonClickPacket
import wtf.gofancy.mc.ematter.features.mad.common.network.MadRecipeSwitchButtonClickPacketHandler
import wtf.gofancy.mc.ematter.features.mad.common.recipe.capability.MadRecipeCapabilityHandler
import wtf.gofancy.mc.ematter.features.mad.common.recipe.step.*
import wtf.gofancy.mc.ematter.util.laro

object MolecularAssemblerDevice : ModFeature {

    private const val NAME = "molecular_assembler_device"

    internal val block by laro(this.NAME) {
        MadBlock().apply {
            this.setCreativeTab(BaseMod.itemGroup)
            this.setTranslationKey("ematter.molecular_assembler_device")
            this.setHardness(8.0F)
            this.setHarvestLevel("pickaxe", 2)
        }
    }

    internal val item by laro(this.NAME) {
        ItemMultiTexture(this.block, this.block) { MadTier.fromMeta(it.metadata).translationKey }
    }

    internal object SteppingFunctions {
        val registry: IForgeRegistry<SteppingFunctionSerializer> by lazy {
            RegistryManager.ACTIVE.getRegistry(SteppingFunctionSerializer::class.java)
        }

        val constant by laro("constant", ::ConstantSteppingFunctionSerializer)
        val exponential by laro("exponential", ::ExponentialSteppingFunctionSerializer)
        val linear by laro("linear", ::LinearSteppingFunctionSerializer)
        val piecewise by laro("piecewise", ::PiecewiseSteppingFunctionSerializer)
        val quadratic by laro("quadratic", ::QuadraticSteppingFunctionSerializer)
    }

    override val client: () -> () -> ModFeature.ClientFeature
        get() = { { MolecularAssemblerDeviceClient } }

    override val customRegistryBuilders: List<RegistryBuilder<*>>
        get() = listOf(
            RegistryBuilder<SteppingFunctionSerializer>()
                .setName(ResourceLocation(MOD_ID, "stepping_function_serializers"))
                .setType(SteppingFunctionSerializer::class.java)
                .setMaxID(67_108_863)
                .disableSaving()
                .allowModification()
        )

    override fun registerRegistrars() {
        MinecraftForge.EVENT_BUS.apply {
            register(Registrar)
        }
    }

    override fun registerCapabilities() {
        MadRecipeCapabilityHandler.registerCapability()
    }

    override fun registerEventHandlers() {
        MinecraftForge.EVENT_BUS.apply {
            register(MadRecipeCapabilityHandler)
        }
    }

    override fun registerNetworkMessages() {
        ModNetworkManager.registerMessage(
            MadRecipeCapabilitySyncPacket::class,
            MadRecipeCapabilitySyncPacketHandler::class,
            Side.CLIENT
        )
        ModNetworkManager.registerMessage(
            MadRecipeSwitchButtonClickPacket::class,
            MadRecipeSwitchButtonClickPacketHandler::class,
            Side.SERVER
        )
    }

    @Suppress(
        "Unused",
        "RemoveRedundantQualifierName"
    )
    private object Registrar {

        @SubscribeEvent
        fun registerBlocks(event: RegistryEvent.Register<Block>) {
            event.registry.registerAll(MolecularAssemblerDevice.block)

            /*
             * Hijack block registry event for block entity registration. I imagine that this is not the most correct
             * way to do it, but in lack of a better place this works just fine.
             */
            GameRegistry.registerTileEntity(
                MadBlockEntity::class.java,
                ResourceLocation(MOD_ID, MolecularAssemblerDevice.NAME)
            )
        }

        @SubscribeEvent
        fun registerItems(event: RegistryEvent.Register<Item>) {
            event.registry.registerAll(MolecularAssemblerDevice.item)
        }

        @SubscribeEvent
        fun register(event: RegistryEvent.Register<SteppingFunctionSerializer>) {
            event.registry.registerAll(
                MolecularAssemblerDevice.SteppingFunctions.constant,
                MolecularAssemblerDevice.SteppingFunctions.exponential,
                MolecularAssemblerDevice.SteppingFunctions.linear,
                MolecularAssemblerDevice.SteppingFunctions.piecewise,
                MolecularAssemblerDevice.SteppingFunctions.quadratic
            )
        }
    }
}
