/*
 * Copyright (C) 2021  TheSilkMiner
 *
 * This file is part of Boson.
 *
 * Boson is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Boson is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Boson.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Contact information:
 * E-mail: thesilkminer <at> outlook <dot> com
 */

@file:JvmName("LocaleUtils")

package wtf.gofancy.mc.ematter.util

import net.minecraft.client.resources.I18n
import net.minecraft.util.text.TextFormatting

enum class Color {
    DEFAULT,
    AQUA,
    BLACK,
    BLUE,
    DARK_AQUA,
    DARK_BLUE,
    DARK_GRAY,
    DARK_GREEN,
    DARK_PURPLE,
    DARK_RED,
    GOLD,
    GRAY,
    GREEN,
    PURPLE,
    RED,
    WHITE,
    YELLOW
}

enum class Style {
    NORMAL,
    BOLD,
    ITALIC,
    UNDERLINE,
    STRIKE_THROUGH
}

enum class Readability {
    READABLE,
    OBFUSCATED
}

fun String.toLocale(
    vararg arguments: Any?,
    color: Color = Color.DEFAULT,
    style: Style = Style.NORMAL,
    readability: Readability = Readability.READABLE
) =
    localizeAndFormat(this, color, style, readability, *arguments)

fun localizeAndFormat(
    message: String,
    color: Color,
    style: Style,
    readability: Readability,
    vararg arguments: Any?
): String =
    runSided(
        server = { { message } },
        client = { { I18n.format(message, *arguments).apply(color, style, readability) } }
    )

private fun String.apply(color: Color, style: Style, readability: Readability): String {
    fun Color.toTextFormatting() = when (this) {
        Color.DEFAULT -> null
        Color.AQUA -> TextFormatting.AQUA
        Color.BLACK -> TextFormatting.BLACK
        Color.BLUE -> TextFormatting.BLUE
        Color.DARK_AQUA -> TextFormatting.DARK_AQUA
        Color.DARK_BLUE -> TextFormatting.DARK_BLUE
        Color.DARK_GRAY -> TextFormatting.DARK_GRAY
        Color.DARK_GREEN -> TextFormatting.DARK_GREEN
        Color.DARK_PURPLE -> TextFormatting.DARK_PURPLE
        Color.DARK_RED -> TextFormatting.RED
        Color.GOLD -> TextFormatting.GOLD
        Color.GRAY -> TextFormatting.GRAY
        Color.GREEN -> TextFormatting.GREEN
        Color.PURPLE -> TextFormatting.LIGHT_PURPLE
        Color.RED -> TextFormatting.RED
        Color.WHITE -> TextFormatting.WHITE
        Color.YELLOW -> TextFormatting.YELLOW
    }

    fun Style.toTextFormatting() = when (this) {
        Style.NORMAL -> null
        Style.BOLD -> TextFormatting.BOLD
        Style.ITALIC -> TextFormatting.ITALIC
        Style.UNDERLINE -> TextFormatting.UNDERLINE
        Style.STRIKE_THROUGH -> TextFormatting.STRIKETHROUGH
    }

    fun Readability.toTextFormatting() = when (this) {
        Readability.READABLE -> null
        Readability.OBFUSCATED -> TextFormatting.OBFUSCATED
    }

    val platformColor = color.toTextFormatting()?.toString() ?: ""
    val platformStyle = style.toTextFormatting()?.toString() ?: ""
    val platformReadability = readability.toTextFormatting()?.toString() ?: ""

    if (platformStyle.isNotEmpty() && platformReadability.isNotEmpty()) {
        throw IllegalArgumentException("Both style $style and readability $readability were specified for text '$this': this is not currently supported.")
    }

    val reset =
        if (platformColor.isNotEmpty() || platformReadability.isNotEmpty() || platformStyle.isNotEmpty()) TextFormatting.RESET.toString() else ""

    return "$platformColor${if (platformReadability.isNotEmpty()) platformReadability else platformStyle}$this$reset"
}
