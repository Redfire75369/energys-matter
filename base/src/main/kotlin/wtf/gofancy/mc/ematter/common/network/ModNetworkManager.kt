package wtf.gofancy.mc.ematter.common.network

import net.minecraftforge.fml.common.network.NetworkRegistry
import net.minecraftforge.fml.common.network.simpleimpl.IMessage
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler
import net.minecraftforge.fml.relauncher.Side
import wtf.gofancy.mc.ematter.MOD_ID
import kotlin.reflect.KClass

object ModNetworkManager {

    private var messageDiscriminationCounter = 0

    internal val channel by lazy { NetworkRegistry.INSTANCE.newSimpleChannel(MOD_ID) }

    internal fun <M : IMessage, R : IMessage?> registerMessage(
        message: KClass<M>,
        handler: KClass<out IMessageHandler<M, R>>,
        receivingSide: Side
    ) {
        channel.registerMessage(handler.java, message.java, messageDiscriminationCounter, receivingSide)
        this.messageDiscriminationCounter++
    }
}
