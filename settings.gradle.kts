pluginManagement {
    repositories {
        exclusiveContent {
            forRepository {
                maven {
                    name = "GTNH Maven"
                    url = uri("https://nexus.gtnewhorizons.com/repository/public/")

                }
            }
            filter {
                includeGroup("com.gtnewhorizons")
                includeGroup("com.gtnewhorizons.retrofuturagradle")
            }
        }
        exclusiveContent {
            forRepository {
                maven {
                    name = "Garden of Fancy Maven"
                    url = uri("https://maven.gofancy.wtf/snapshots")
                }
            }
            filter {
                includeGroupAndSubgroups("wtf.gofancy.gradle")
            }
        }

        gradlePluginPortal()
        mavenCentral()
    }
}

plugins {
    // Apply the foojay-resolver plugin to allow automatic download of JDKs
    id("org.gradle.toolchains.foojay-resolver-convention") version "0.8.0"
}

rootProject.name = "Energy's Matter"
include("base")
